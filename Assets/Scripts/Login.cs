using UnityEngine;
using System.Collections;

public class Login : Photon.MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    PhotonNetwork.autoJoinLobby = false;
	    PhotonNetwork.ConnectUsingSettings("1.0");
	}

    void Update()
    {
        if (NetworkApi.Instance.IsLoggedIn)
        {
            Loading.Load(LoadingScene.Lobby);
        }
    }

    void OnConnectedToMaster()
    {
       Auth();
    }

    void OnGUI()
    {
        GUILayout.Label("Login...");
    }

    private void Auth()
    {
        Storage.Login = PlayerPrefs.GetString("userLogin", "usr" + Random.Range(0, int.MaxValue));
        PlayerPrefs.SetString("userLogin", Storage.Login);
        PlayerPrefs.Save();

        StartCoroutine(NetworkApi.Instance.Login(Storage.Login));
    }
}
